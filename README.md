Our business is exclusively Solar! We will make sure you feel comfortable and educated on each step along the solar path. It is a long term commitment to put solar panels on your roof and we want to ensure its the right choice for you. With over 1,500 installs we have learnt about all the small details that will give you confidence the job is done right. Proposals, Government rebates, Financing, Design, Installation and long term client care are all factors we are here to help you through.

Website: https://sunly.ca/
